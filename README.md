#secure_crt_python_script
本项目是使用sercureCrt维护Linux服务器下tomcat常用脚本
包含
1. 强制杀掉tomcat命令
2. 重启Tomcat
3. 替换文件中内容
4. 校验服务器中文件指定行是否为正确配置
5. 读取ip文件
6. 登录多台服务器进行操作，将执行结果保存至日志文件中

#说明
执行方式为SercureCrt--->Script--->Run选择脚本进行执行
ip文件以换行符分隔
